import { Person } from "./person.js";
import Employee from "./employee.js";
let person1 = new Person("Bao", 18, "male");
console.log(person1.getPersonInfo());
console.log(person1 instanceof Person)

let person2 = new Person("Gia", 38, "female");
console.log(person2.getPersonInfo());
console.log(person2 instanceof Person);

let person3 = new Person("Nam", 28, "male");
console.log(person3.getPersonInfo());
console.log(person3 instanceof Person);


let employee1 = new Employee("Hoang", 19, "male", "ABC Co", "3000", "Engineer");
console.log(employee1.getPersonInfo());
console.log(employee1.getEmployeeInfo());
console.log(employee1 instanceof Employee)
employee1.setSalary(9999)
console.log(employee1.getSalary())


let employee2 = new Employee("Minh", 29, "female", "IronHackVN", "5000", "Developer");
console.log(employee2.getPersonInfo())
console.log(employee2.getEmployeeInfo())
console.log(employee2 instanceof Employee)


let employee3 = new Employee("Thao", 29, "female", "XXX company", "7000", "Staff");
console.log(employee3.getPersonInfo())
console.log(employee3.getEmployeeInfo())
console.log(employee3 instanceof Employee)