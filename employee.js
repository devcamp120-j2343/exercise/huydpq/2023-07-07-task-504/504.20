import { Person } from "./person.js";

class Employee extends Person {
    constructor(personName, personAge, gender, employer, salary, position) {
        super(personName, personAge, gender);
        this.employer = employer;
        this.salary = salary;
        this.position = position

    }
    getEmployeeInfo() {
        return `Name: ${this.personName}, Age: ${this.personAge}, Gender: ${this.gender}, Employer: ${this.employer}, Salary: ${this.salary}, Position: ${this.position}`;
    }
    getSalary() {
        return `Salary: ${this.salary}`
    }
    setSalary(paramNewSalary) {
        return `New salary: ${this.salary = paramNewSalary}`
    }
    getPosition() {
        return `Position: ${this.position}`
    }
    setSalary(paramNewPosition) {
        return `New position: ${this.position = paramNewPosition}`
    }
}
export default Employee
